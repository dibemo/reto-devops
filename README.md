# INSTRUCCIONES RETOS REALIZADOS
### Reto 1. Dockerize la aplicación

Se crea directorio node-app donde se aloja el codigo de la aplicacion + el archivo Dockerfile
que dockeriza la aplicacion. Se construye una imagen pequeña a partir de node-alpine y se ejecuta la aplicacion con usuario node.
Para construir la imagen ejecutar el siguiente comando docker:

**docker build -t [nombreImagen:tag] .**

Para levantar el contenedor ejecutar el siguiente comando:

**docker run -d -p 3000:3000 [imagen]**

### Reto 2. Docker Compose

Se crea directorio nginx-web para generar contenedor nginx como proxy reverso, ademas de asegurar con auth_basic, habilitar https y redireccionar trafico de 80 >>>443
En este directorio se aloja el codigo necesario(conf, cert, dockerfile) para generar el contenedor nginx.

En la raiz del repositorio se genera archivo docker-compose.yml que permite levantar los contenedores node y nginx.

Para levantar ambos contenedores ejecutar:

**docker-compose up -d**


### Reto 3. Probar la aplicación en cualquier sistema CI/CD

En la raiz del repositorio se genera archivo .gitlab-ci.yml que permite ejecutar pipeline en gitlab-ci.
Se generan 2 stages para realizar deploy mediante docker-compose y deploy en kubernetes del manifiesto que se generó para el reto 4.

### Reto 4. Deploy en kubernetes

Se genera archivo de implementacion (deploy-nodeapp.yaml) para poder implementar la aplicacion nodejs.
Se genera deployment, service y horizontal pod autoscaler.

Para generar el deployment ejecutar:

**kubectl apply -f deploy-nodeapp.yaml**

Ademas se incorpora este deploy en archivo .gitlab-ci.yml para realizarlo mediante pipeline automatizado.


***NOTA:***
Es necesario contar con algunos requisitos previos:
- Tener cuenta docker y tener instalado docker en ambiente local.
- Realizar la instalacion y la configuracion del runner en gitlab-ci
- Tener instalado minikube para desplegar el deploy kubernetes.


    


















